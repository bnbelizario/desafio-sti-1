# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
user = User.create(name: 'Bruno Belizario', email: 'bnbelizario@id.uff.br')
cart = Cart.create(status: 'buying', user: user)

p1 = Product.create(name: "Iphone", description: "XS 64 GB", price: 6000.0)
p2 = Product.create(name: "Notebook Dell",
                    description: "16 GB RAM, core i7, 1 TB HD", price: 3500.0)
