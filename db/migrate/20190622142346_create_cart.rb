class CreateCart < ActiveRecord::Migration[5.2]
  def change
    create_table :carts do |t|
      t.string :status
      t.float :total_price
      t.timestamps null: false
      t.references :user, null: false, foreign_key: true
    end
  end
end
