class CartWorker
  include Sidekiq::Worker

  def perform(item_hash)
    Item.create(JSON.load(item_hash))
  end
end
