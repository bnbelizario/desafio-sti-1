class ItemsController < ApplicationController
  def create
    @item = Item.new(item_parameters)
    item_json = @item.serializable_hash.to_json
    item_job = CartWorker.perform_in(10.seconds, item_json)
    flash[:item_job] = item_job
    redirect_to root_url
  end

  def destroy
    @item = Item.find(item_parameters[:id])
    @item.destroy
    redirect_to cart_path(1)
  end

  def undo
    ss = Sidekiq::ScheduledSet.new
    jobs = ss.select { |job| job.jid == item_parameters[:item_job] }
    jobs.each(&:delete)
    redirect_to root_path
  end

  private

  def item_parameters
    params.permit(:id, :quantity, :product_id, :item_job)
        .merge(cart_id: 1)
  end
end