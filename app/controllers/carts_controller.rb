class CartsController < ApplicationController
  def show
    @cart = Cart.find(params[:id])
  end

  private

  def cart_parameters
    params.require(:cart).permit(:id)
  end
end