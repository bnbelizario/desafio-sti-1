class Cart < ApplicationRecord
  belongs_to :user
  has_many :items
  has_many :products, through: :items

  def total_price
    total = 0
    items.map { |item| total += item.product.price * item.quantity }
    total
  end
end