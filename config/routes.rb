require 'sidekiq/web'
Sidekiq::Web.set :session_secret, Rails.application.secrets[:secret_key_base]

Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'home#index'
  mount Sidekiq::Web => '/sidekiq'
  resources :carts, only: [:show]
  resources :items, only: [:create, :show, :destroy] do
    collection do
      post :undo
    end
  end
end
