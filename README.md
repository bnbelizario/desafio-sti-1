# README

Para executar a aplicação precisa-se:

1- Criar o banco: rake db:create
2- Criar a estrutura do banco: rake db:migrate
3- Popular o banco: rake db:seed
4- Instanciar um redis (pode ser feito em docker): docker run -d --name redis-desafio -p 6379:6379 redis
5- Subir um Sidekiq: bundle exec sidekiq
